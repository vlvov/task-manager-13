package ru.t1.vlvov.tm.repository;

import ru.t1.vlvov.tm.api.repository.ICommandRepository;
import ru.t1.vlvov.tm.constant.ArgumentConst;
import ru.t1.vlvov.tm.constant.TerminalConst;
import ru.t1.vlvov.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    private final static Command ABOUT = new Command(TerminalConst.ABOUT, ArgumentConst.ABOUT,"Show developer info.");

    private final static Command EXIT = new Command(TerminalConst.EXIT, null, "Close application.");

    private final static Command HELP = new Command(TerminalConst.HELP, ArgumentConst.HELP, "Show application commands.");

    private final static Command VERSION = new Command(TerminalConst.VERSION, ArgumentConst.VERSION, "Show application version.");

    private final static Command INFO = new Command(TerminalConst.INFO, ArgumentConst.INFO, "Show available memory.");

    private final static Command COMMANDS = new Command(TerminalConst.COMMANDS, ArgumentConst.COMMANDS, "Show commands.");

    private final static Command ARGUMENTS = new Command(TerminalConst.ARGUMENTS, ArgumentConst.ARGUMENTS, "Show arguments.");

    private final static Command PROJECT_CREATE = new Command(TerminalConst.PROJECT_CREATE, "", "Create new project.");

    private final static Command PROJECT_CLEAR = new Command(TerminalConst.PROJECT_CLEAR, "", "Remove all projects.");

    private final static Command PROJECT_LIST = new Command(TerminalConst.PROJECT_LIST, "", "Display all projects.");

    private final static Command TASK_CREATE = new Command(TerminalConst.TASK_CREATE, "", "Create new task.");

    private final static Command TASK_CLEAR = new Command(TerminalConst.TASK_CLEAR, "", "Remove all tasks.");

    private final static Command TASK_LIST = new Command(TerminalConst.TASK_LIST, "", "Display all tasks.");

    private final static Command SHOW_PROJECT_BY_ID = new Command(TerminalConst.PROJECT_SHOW_BY_ID, "", "Show project by Id.");

    private final static Command SHOW_PROJECT_BY_INDEX = new Command(TerminalConst.PROJECT_SHOW_BY_INDEX, "", "Show project by Index.");

    private final static Command REMOVE_PROJECT_BY_ID = new Command(TerminalConst.PROJECT_REMOVE_BY_ID, "", "Remove project by Id.");

    private final static Command REMOVE_PROJECT_BY_INDEX = new Command(TerminalConst.PROJECT_REMOVE_BY_INDEX, "", "Remove project by Index.");

    private final static Command UPDATE_PROJECT_BY_ID = new Command(TerminalConst.PROJECT_UPDATE_BY_ID, "", "Update project by Id.");

    private final static Command UPDATE_PROJECT_BY_INDEX = new Command(TerminalConst.PROJECT_UPDATE_BY_INDEX, "", "Update project by Index.");

    private final static Command SHOW_TASK_BY_ID = new Command(TerminalConst.TASK_SHOW_BY_ID, "", "Show task by Id.");

    private final static Command SHOW_TASK_BY_INDEX = new Command(TerminalConst.TASK_SHOW_BY_INDEX, "", "Show task by Index.");

    private final static Command REMOVE_TASK_BY_ID = new Command(TerminalConst.TASK_REMOVE_BY_ID, "", "Remove task by Id.");

    private final static Command REMOVE_TASK_BY_INDEX = new Command(TerminalConst.TASK_REMOVE_BY_INDEX, "", "Remove task by Index.");

    private final static Command UPDATE_TASK_BY_ID = new Command(TerminalConst.TASK_UPDATE_BY_ID, "", "Update task by Id.");

    private final static Command UPDATE_TASK_BY_INDEX = new Command(TerminalConst.TASK_UPDATE_BY_INDEX, "", "Update task by Index.");

    private final static Command PROJECT_CHANGE_STATUS_BY_ID = new Command(TerminalConst.PROJECT_CHANGE_STATUS_BY_ID, "", "Change project status by Id.");

    private final static Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(TerminalConst.PROJECT_CHANGE_STATUS_BY_INDEX, "", "Change project status by Index.");

    private final static Command PROJECT_COMPLETE_BY_ID = new Command(TerminalConst.PROJECT_COMPLETE_BY_ID, "", "Complete project by Id.");

    private final static Command PROJECT_COMPLETE_BY_INDEX = new Command(TerminalConst.PROJECT_COMPLETE_BY_INDEX, "", "Complete project by Index.");

    private final static Command PROJECT_START_BY_ID = new Command(TerminalConst.PROJECT_START_BY_ID, "", "Start project by Id.");

    private final static Command PROJECT_START_BY_INDEX = new Command(TerminalConst.PROJECT_START_BY_INDEX, "", "Start project by Index.");

    private final static Command TASK_CHANGE_STATUS_BY_ID = new Command(TerminalConst.TASK_CHANGE_STATUS_BY_ID, "", "Change task status by Id.");

    private final static Command TASK_CHANGE_STATUS_BY_INDEX = new Command(TerminalConst.TASK_CHANGE_STATUS_BY_INDEX, "", "Change task status by Index.");

    private final static Command TASK_COMPLETE_BY_ID = new Command(TerminalConst.TASK_COMPLETE_BY_ID, "", "Complete task by Id.");

    private final static Command TASK_COMPLETE_BY_INDEX = new Command(TerminalConst.TASK_COMPLETE_BY_INDEX, "", "Complete task by Index.");

    private final static Command TASK_START_BY_ID = new Command(TerminalConst.TASK_START_BY_ID, "", "Start task by Id.");

    private final static Command TASK_START_BY_INDEX = new Command(TerminalConst.TASK_START_BY_INDEX, "", "Start task by Index.");

    private final static Command TASK_BIND_TO_PROJECT = new Command(TerminalConst.TASK_BIND_TO_PROJECT, "", "Bind task to Project.");

    private final static Command TASK_UNBIND_FROM_PROJECT = new Command(TerminalConst.TASK_UNBIND_FROM_PROJECT, "", "Unbind task from Project.");

    private final static Command TASK_SHOW_BY_PROJECT_ID = new Command(TerminalConst.TASK_SHOW_BY_PROJECT_ID, "", "Show tasks by Project Id.");

    private final static Command[] TERMINAL_COMMANDS = new Command[] {
        ABOUT, EXIT, HELP, VERSION, INFO, COMMANDS, ARGUMENTS, PROJECT_CREATE, PROJECT_CLEAR,
        PROJECT_LIST, TASK_CREATE, TASK_CLEAR, TASK_LIST,
        UPDATE_PROJECT_BY_INDEX,UPDATE_PROJECT_BY_ID, REMOVE_PROJECT_BY_INDEX, REMOVE_PROJECT_BY_ID,
        SHOW_PROJECT_BY_INDEX, SHOW_PROJECT_BY_ID,
        UPDATE_TASK_BY_INDEX, UPDATE_TASK_BY_ID, REMOVE_TASK_BY_INDEX, REMOVE_TASK_BY_ID,
        SHOW_TASK_BY_INDEX, SHOW_TASK_BY_ID,
        PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_COMPLETE_BY_ID,
        PROJECT_COMPLETE_BY_INDEX, PROJECT_START_BY_ID, PROJECT_START_BY_INDEX,
        TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX, TASK_COMPLETE_BY_ID,
        TASK_COMPLETE_BY_INDEX, TASK_START_BY_ID, TASK_START_BY_INDEX,
        TASK_BIND_TO_PROJECT, TASK_UNBIND_FROM_PROJECT, TASK_SHOW_BY_PROJECT_ID
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
