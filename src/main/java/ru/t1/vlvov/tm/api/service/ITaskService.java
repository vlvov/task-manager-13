package ru.t1.vlvov.tm.api.service;

import ru.t1.vlvov.tm.enumerated.Status;
import ru.t1.vlvov.tm.model.Project;
import ru.t1.vlvov.tm.model.Task;
import java.util.List;

public interface ITaskService {

    Task add(Task task);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

    Task create(String name);

    Task create(String name, String description);

    void clear();

    List<Task> showAll();

    List<Task> findAllByProjectId(String projectId);

    void remove(Task task);

    Task removeById(String id);

    Task removeByIndex(Integer index);

    Task findOneById(String id);

    Task findOneByIndex(Integer index);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

}
