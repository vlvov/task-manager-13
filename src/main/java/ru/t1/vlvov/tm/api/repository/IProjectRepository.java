package ru.t1.vlvov.tm.api.repository;

import ru.t1.vlvov.tm.model.Project;

import java.util.List;

public interface IProjectRepository {

    Project add(Project project);

    void clear();

    boolean existsById(String projectId);

    List<Project> showAll();

    Project findOneById(String id);

    Project findOneByIndex(Integer index);

    void remove(Project project);

    Project removeById(String id);

    Project removeByIndex(Integer index);

}
